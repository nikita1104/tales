import { config } from "../default/config";
import { colors } from "../default/colors";

export interface tileType {
  id: number;
  color: string;
  isOpen: boolean;
  isComplete: boolean;
}

export type baseData = Array<tileType>;

export const defaultTiles: baseData = Array.from(
  { length: Math.pow(config.baseCount, 2) },
  (_, index) => ({
    id: index,
    color: colors[index],
    isOpen: false,
    isComplete: false,
  })
).sort(() => Math.random() - 0.5); //color randomize;
