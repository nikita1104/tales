import { config } from "./config";
import { colorsArray } from "./colorsArray";

export const colors = colorsArray.reduce(
  (m: string[], i) =>
    m.concat(Array.from({ length: config.repeatColors }, () => i)), //color duplication
  []
);
