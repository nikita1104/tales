import React from "react";
import "./App.scss";
import { createSubarray } from "./helpers/createSubarray";
import { config } from "./default/config";
import { connect } from "react-redux";
import { Table } from "./components/Table";
import { tileType } from "./default/tiles";

function App({
  tiles,
  inProgress,
}: {
  tiles: tileType[];
  inProgress: boolean;
}) {
  const rows = createSubarray(tiles, config.baseCount);
  return (
    <div className="App">
      {inProgress ? <Table tiles={rows} /> : "You Win ;)"}
    </div>
  );
}

const mapStateToProps = (state: {
  dataReducer: tileType[];
  inProgressReducer: boolean;
}) => {
  return {
    tiles: state.dataReducer,
    inProgress: state.inProgressReducer,
  };
};

export default connect(mapStateToProps)(App);
