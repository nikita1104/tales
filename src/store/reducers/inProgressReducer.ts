import { SET_WIN } from "../types";

export const inProgressReducer = (
  state = true,
  action: { type: string; payload?: any }
) => {
  switch (action.type) {
    case SET_WIN:
      return false;
    default:
      return state;
  }
};
