import { defaultTiles, tileType } from "../../default/tiles";
import { SELECT_TILE, UPDATE_TILES } from "../types";

export const clickWorker = (id: number, state: tileType[]) => {
  let arr = state.map((x: tileType) => ({
    ...x,
    isOpen: x.id === id && !x.isComplete ? !x.isOpen : x.isOpen,
  }));
  return arr;
};

export const dataReducer = (
  state = defaultTiles,
  action: { type: string; payload?: any }
) => {
  switch (action.type) {
    case SELECT_TILE:
      return clickWorker(action.payload, state);
    case UPDATE_TILES:
      return action.payload;
    default:
      return state;
  }
};
