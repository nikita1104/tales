import { combineReducers } from "redux";
import { dataReducer } from "./gameData";
import { inProgressReducer } from "./inProgressReducer";

export const rootReducer = combineReducers({
  dataReducer,
  inProgressReducer,
});
