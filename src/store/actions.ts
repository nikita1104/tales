import { SELECT_TILE } from "./types";

export const tileCLickHandler = (id: number) => ({
  type: SELECT_TILE,
  payload: id,
});

export const actions = { tileCLickHandler };
