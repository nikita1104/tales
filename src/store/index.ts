import { tileType } from "../default/tiles";
import { config } from "../default/config";
import { UPDATE_TILES, SET_WIN } from "./types";
import { createStore } from "redux";
import watch from "redux-watch";
import { rootReducer } from "./reducers";

const checker = (arr: tileType[]): { status: string; data?: tileType[] } => {
  const countOfOpened = arr.filter((tile) => tile.isOpen).length;
  if (arr.filter((x) => !x.isComplete).length === 0) {
    return { status: "win" };
  }
  if (countOfOpened === config.repeatColors) {
    const opened = arr.filter((x) => x.isOpen);
    if (opened[0].color === opened[1].color) {
      return {
        status: "need_update",
        data: arr.map((x) => ({
          ...x,
          isComplete: x.isOpen || x.isComplete ? true : false,
          isOpen: false,
        })),
      };
    } else {
      return {
        status: "reset_opened",
        data: arr.map((x) => ({
          ...x,
          isOpen: false,
        })),
      };
    }
  } else {
    return {
      status: "оставляем как есть",
    };
  }
};

export const store = createStore(rootReducer);

let w = watch(store.getState);
store.subscribe(
  w(({ dataReducer }) => {
    setTimeout(() => {
      const check = checker(dataReducer);
      if (check.status === "need_update") {
        store.dispatch({ type: UPDATE_TILES, payload: check.data });
      } else if (check.status === "win") {
        store.dispatch({ type: SET_WIN });
      } else if (check.status === "reset_opened") {
        store.dispatch({ type: UPDATE_TILES, payload: check.data });
      }
    }, 500);
  })
);
