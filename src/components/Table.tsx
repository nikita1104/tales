import React from "react";
import { Tile } from "./Tile";
import { tileType } from "../default/tiles";

export const Table = ({ tiles }: { tiles: Array<tileType[]> }) => (
  <div className="table">
    {tiles.map((row, i) => (
      <div className="row" key={i}>
        {row.map((col, i) => (
          <Tile
            color={col.color}
            id={col.id}
            isOpen={col.isOpen}
            isComplete={col.isComplete}
            key={i}
          />
        ))}
      </div>
    ))}
  </div>
);
