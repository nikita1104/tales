import React from "react";
import { connect } from "react-redux";
import { actions } from "../store/actions";
import classNames from "classnames";

export const Component = ({
  color,
  id,
  clickHandle,
  isOpen,
  isComplete,
}: {
  color: string;
  id: number;
  clickHandle: any;
  isOpen: boolean;
  isComplete: boolean;
}) => {
  var tileClass = classNames({
    tile: true,
    active: !isComplete,
  });
  return (
    <div
      className={tileClass}
      style={{ backgroundColor: isOpen || isComplete ? color : "#eee" }}
      onClick={() => clickHandle(id)}
    />
  );
};

const dispatchToProps = (
  dispatch: (arg0: { type: string; payload: number }) => any
) => ({
  clickHandle: (tileId: number) => dispatch(actions.tileCLickHandler(tileId)),
});

export const Tile = connect(null, dispatchToProps)(Component);
